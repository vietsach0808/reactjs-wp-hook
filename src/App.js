import logo from './logo.svg';
import logo2 from './logo2.png';
import './App.css';
import React from "react";
import { createHooks } from '@wordpress/hooks';

let globalHooks = createHooks();

class App extends React.Component {
  constructor(props) {
    super(props);
    // globalHooks.addFilter( 'header_output', 'myApp', this.renderHeader );
    globalHooks.addFilter('after_header', 'myApp', this.renderAfterHeader);
    // globalHooks.addFilter( 'should_learn', 'myApp', this.shouldNotLearnReact );
    // globalHooks.addAction( 'before_output', 'myApp', this.youShouldLearnReact );
    // globalHooks.addFilter( 'header_logo', 'myApp', this.newLogo );
    // globalHooks.addAction( 'logo_changed', 'myApp', this.notify );

  };

  componentWillMount() {
    console.log("componentWillMount");

  }

  componentDidMount() {
    console.log("componentDidMount");
  }


  renderAfterHeader(content) {
    console.log('----------- hook renderAfterHeader -----------');
    content.push(<h1 key="headerH1">This is After Header</h1>);
    return content;
  }

  shouldNotLearnReact(bool) {
    return false;
  }

  youShouldLearnReact(bool) {
    globalHooks.removeFilter('should_learn', 'myApp', 'shouldNotLearnReact');
  }

  newLogo(logo) {
    if (logo !== logo2) {
      globalHooks.doAction('logo_changed', logo2);
      return logo2;
    }
    return logo;
  }

  notify(logo) {
    if (logo === logo2) {
      // console.log('Changed logo');
    }
  }

  renderHeader(header) {
    // Applying filters to change the image. Default is the logo.svg.
    let logoImage = globalHooks.applyFilters('header_logo', logo);

    // Applying filters to see if we should or not learn React. Default is true.
    let shouldLearnReact = globalHooks.applyFilters('should_learn', true);

    let header_html = <header key="header" className="App-header">
      <img src={logoImage} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      {shouldLearnReact && <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>}
    </header>;
    header.push(header_html);
    return header;
  }

  render() {
    // globalHooks.doAction( 'before_output' );
    let output = [];
    // output.push( globalHooks.applyFilters( 'start_output', [] ) );
    // output.push( globalHooks.applyFilters( 'header_output', [] ) );
    output.push(globalHooks.applyFilters('after_header', []));
    return (
      <div className="App">
        {output}
      </div>
    );
  }
}

export default App;
